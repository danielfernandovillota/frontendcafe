import { useState } from 'react'
import './App.css'
import { Header } from './components/Header'
import { Table } from './components/Table'

function App() {
  

  return (
    <div className='bg-red-900 h-screen w-screen'>
      <Header/>
      <Table/>
    </div>
      
  )
}

export default App
