import React from 'react'
import axios from 'axios';

export const Table = () => {

    const url = "http://localhost:8000/cafe/miproyecto";
    const [post, setPost] = React.useState([]);

    React.useEffect(() => {
      axios.get(url).then((response) => {
        setPost(response.data);
        console.log(response.data)
      });
    }, []);

    const contador = post.reduce((count, item) => {
        count[item.tipo] = (count[item.tipo] || 0) + 1;
        return count;
      }, {});

  return (
    <div>
    <table className="table-auto border-collapse border border-red-950 w-full h-full text-center ">
        <thead>
          <tr className='text-white'>
            <th className="bg-red-950 border border-red-950">Nombre</th>
            <th className="bg-red-950 border border-red-950">Tipo</th>
            <th className="bg-red-950 border border-red-950">Region</th>
          </tr>
        </thead>
        <tbody>
          {post.map((item) => (
            <tr key={item.id} className='bg-white'>
              <td className="py-4 border border-red-950">{item.nombre}</td>
              <td className="py-4 border border-red-950">{item.tipo}</td>
              <td className="py-4 border border-red-950">{item.region}</td>
            </tr>
          ))}
        </tbody>
        
    </table>
    <div className='bg-red-900 text-white'>
        {Object.keys(contador).map((tipo) => (
        <div key={tipo}>
        Tipo Cafe: {tipo} - Cantidad: {contador[tipo]}
        </div>
        ))}
    </div>
    </div>
  )
}
export default Table
