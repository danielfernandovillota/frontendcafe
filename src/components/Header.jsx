import React from 'react'

export const Header = () => {
  return (
  
    <header className='flex flex-col gap-2 '>
        <h1 className='text-4xl text-left text-white'>El Aroma Mágico</h1>
        <img className='w-screen h-[350px]' src='https://ironbankcoffee.com/wp-content/uploads/2013/12/header-coffee.jpg' />
    </header>
 
  )
}
